﻿$('#gale').click(function () {
    $('#GaleriaFotos').fadeIn(500);
});

$('#BtnCloseGaleriaFotos').click(function () {
  
    $('#GaleriaFotos').fadeOut(250);
});

$("#btnVideo").click(function () {
    
    IdVideo = $("#Clave").val();
    $("#vplay").load(rootUrl + "/Productos/VideoPlayer?Src=P&Id=" + IdVideo)
    $(".manto").fadeIn(500);
    $(".videoplay").show();
});


$("#btnEnviar").click(function () {
    EnviaEmail();
});
   


$(".cart").click(function () {
    var Clave = this.id.replace("Cve", "");
    AgregaCarrito(Clave, 1);         
});

$(".estrella").click(function () {
    var Clave = this.id.replace("Cve", "");
    AgregaFavoritos(Clave);
});


$("#face").click(function () {
    var Clave = $("#Clave").val();
    window.open('https://www.facebook.com/sharer/sharer.php?sdk=joey&u=http://www.dogotuls.com.mx/' + rootUrl + '/Productos/Detalle/' + Clave + '&display=popup&ref=plugin&src=share_button');
});

$("#comprar").click(function () {
    var Clave = $("#Clave").val();
    var Cant = $("#Cantidad").val();
    AgregaCarrito(Clave, Cant);
    //window.location.href = rootUrl + "/Tienda/AgregaCarrito?Clave=" + Clave + "&Cant=" + Cant;
});

$("#catal").click(function () {
    var PagCat = $("#PagCatalog").val();
    window.location.href = rootUrl + "~/Productos/catalogo-ferreteria-herramientas-seguridad-carga-construccion?Pg=" + PagCat;
})
        

$("#email").click(function () {
	if($(window).width()<1025){   $('html,body').animate({scrollTop:'0px'},750);   }
    $("#divEnviaMail").fadeIn(750);
    $('.manto').fadeIn(500);

})


$("#favs").click(function () {
    AgregaFavoritos($("#Clave").val());
});


function EnviaEmail() {
    
 


    $.ajax({
        url: rootUrl + '/Productos/EnviaEmail',
        type: "POST",
        data: {
            nombredestino: $("#nombredestino").val(),
            emaildestino: $("#emaildestino").val(),
            nombreremite: $("#nombreremite").val(),
            emailremite: $("#emailremite").val(),
            Clave: $("#Clave").val(),
            Descripcion: $("#Descripcion").val(), 
            //DescrCompleta: $("#DescrCompleta").val(),
            Comentarios: $("#txtComentarios").val()
        },
        success: function () {
            
            $("#divEnviaMail").fadeOut(600);
            $('.manto').fadeOut(300);
            Confirmacion("Correo enviado con éxito.");

        },
   
    });
}

      $("#btnImprimir").click(function () {                
           var divContents = $(".show").html();
           var printWindow = window.open('', '', 'height=800,width=800');
           printWindow.document.write('<html ><head><title>Dogotuls - Detalle del producto</title>');
           printWindow.document.write('<link href=\'/dogotuls3/estilos.css\' rel=\'stylesheet\' type=\'text/css\'>');
           printWindow.document.write('</head><body class=\'bodyimp\'>');
           printWindow.document.write(divContents);
           printWindow.document.write('</body></html>');
           printWindow.document.close();
           printWindow.print();
      });

$(".btnGaleriaDetalle").hover(function () {
    $(".btnGaleriaDetalle").removeClass('selected');
    $(this).addClass('selected');
    var a = this.id;
    var new_img = '<img src=https://dogotuls.com.mx/media/imagenes/' + a + '>';
    $(".fotoProdDetalle").attr("id", a);
    $(".fotoProdDetalle").html(new_img);
});

$(".btnGaleria").hover(function () {
    $(".btnGaleria").removeClass('selected');
    $(this).addClass('selected');
    var a = this.id;
    var new_img = '<img  width="400" height="400" alt="@Model.Prod.Descripcion"  src="https://dogotuls.com.mx/media/imagenes/' + a + '">';
    $(".fotoProdGaleria").attr("id", a);
    $(".fotoProdGaleria").html(new_img);
});

$(".fotoProd").hover(function () {
    var a = this.id;
    var new_img = '<img src=https://dogotuls.com.mx/media/imagenes/' + a + '>';    
    $(".fotoZoomed").html(new_img);
    $(".fotoZoomed").show();
    $(".descriproduct").hide();
},
    function () {
        $(".fotoZoomed").hide();
        $(".descriproduct").show();
    }

);
       


          