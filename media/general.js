﻿var rootUrl = "/dogotuls3"

var dlgMsg = $("#dialog-message").dialog({
    autoOpen: false,
    modal: true,
    height: 250,
    width: 380,
    buttons: {
        Ok: function () {
            $(this).dialog("close");
        }
    }
});



function Confirmacion(msg, func) {
    $("#lblMsg").html(msg);
    dlgMsg.dialog('option', 'buttons', {

        Ok: function () {
            $(this).dialog("close");
        }
    });
    dlgMsg.dialog("open");
    if (func != null) {
        dlgMsg.on("dialogclose", func)
    }        
}


function ConfirmaCancela(msg, func) {

    $("#lblMsg").html(msg);
    dlgMsg.dialog('option', 'buttons', {
        'Aceptar': function () {
            func();
            dlgMsg.dialog("close");
        },
        'Cancelar': function () {
            dlgMsg.dialog("close");
        }

    }
        );
    dlgMsg.dialog("open"); 
    
}


function BorraFavoritos(cveProd) {
    ConfirmaCancela("¿Está seguro que desea eliminar este producto de sus favoritos?", function () { window.location = rootUrl + '/Productos/BorraFavoritos?CveProducto=' + cveProd;  });     
}



$("#btnSubmitListaCorreos").click(function () {
        
    $.ajax({
        url: rootUrl + '/General/Suscribir',
        type: "POST",
        data: {
            DirCorreo: $("#txtEmail").val(),
            Id: 0
        },
        success: function () {
            Confirmacion("Ha sido agregado a la lista con exito");
            $("#txtEmail").val("");
        },
        error: function () { Confirmacion("Error en dirección de correo") }
    });      
});

$("#buscar").keyup(function (e) {
    if (e.keyCode == 13)
        Busqueda();
});

$("#btnBuscar").click(function () { Busqueda(); });

function Busqueda(){
   
    var Keywords = $("#buscar").val();
    
    
    window.location.href = rootUrl +  "/Productos/Busqueda?Keywords=" + Keywords + "&Pag=0";
   
}

function AgregaFavoritos(cveProd) {
    $.ajax({
        url: rootUrl + '/Productos/AgregaFavoritos',
        type: "GET",
        data: { CveProducto: cveProd },
        error: function (xhr, ajaxOptions, errMsg) {
            if (xhr.status == 401)
                window.location = rootUrl + '/General/Login';
            else
                alert("Error en el servidor favor de reintentar");
        },
        success: function (emsg) { Confirmacion("Producto agregado a sus favoritos") }
    })
}




function AgregaCarrito(cveProd, Cant) {
    $.ajax({
        url: rootUrl + '/Tienda/AgregaCarrito',
        type: "GET",
        data: {
            Clave: cveProd,
            Cant: Cant
        },
        error: function (xhr, ajaxOptions, errMsg) {
            if (xhr.status == 401)
                window.location = rootUrl + '/General/Login';
            else
                alert("Error en el servidor favor de reintentar");
        },
        success: function (emsg) { Confirmacion("Producto agregado al carrito") }
    })
}